// console.log("Hello Batch 288!");

//[Section] JavaScript Synchronous vs Asynchronous
   // JavaScript is by default is synchronous meaning that only one statement is executed a time.

  // this can be provfen when a statement has an error, javascript will not proceed with the next stament.

/*
console.log("Hello World!");

conole.log("Hello after the world");

console.log("hello");*/

   // When certain statements take a lot of time to process, this slows down our code.

   /*for (let index = 0; index <= 1500; index++){
   	   console.log(index);
   }

   console.log("Hello again!");*/

   //Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

//[Section] Getting all posts
    // The Fetch API allows you to asynchronously request for a resourcec data.
    //so it means the fetch method that we are going to use here will run asynchronously.
    // Synatx:
       //fetch('URL');

    //A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

    console.log(fetch('https://jsonplaceholder.typicode.com/posts')); 
    //Syntax:
       /*
          fetch('URL')
          .then((response => response));
       */
    //retrive all posts follow the rest API

    fetch('https://jsonplaceholder.typicode.com/posts')
    //the fetch method will return a promise that resolves the response object.

    // the "then" method captures the response object
    //Use the "Json" method from the response object to convert the data retrive into json fromat to be used in our application
    .then(response => response.json())
    .then(json => console.log(json))

    // the "async" and "await" keyword, it is another aproach that can be used to achieve asynchronous code

    //Creates an asynchronous function

    async function fetchData(){
      
       let result = await fetch ('https://jsonplaceholder.typicode.com/posts');

       console.log(result);

       let json = await result.json();

       console.log(json);
    };

   // fetchData();

   // [Section] Getting specific post

   //Retrives specific post following the rest API (/posts/:id)

   fetch('https://jsonplaceholder.typicode.com/posts/5')
   .then(response => response.json())
   .then(json => console.log(json));

   //[Section] Creating Post
   //Syntax:
        /*
          //options is an object that contains the method, the header and the body of the request

          //by deafult if you don't add the method in the fetch request, it will be a GET method.

          fetch('URL', options)
          .then(response => {})
          .then(response => {})
        */

   fetch('https://jsonplaceholder.typicode.com/posts', {
      //sets the method of the request object to post following the rest API
      method: 'POST',
      //sets the header data of the request object to sent to the backend
      //specified that content will be in JSON structure
      headers: {
         'Content-type': 'application/json'
      },
      //sets the content/body data of the request object to be sent backend
      body: JSON.stringify({
         title : 'New post',
         body: 'Hello World',
         userId: 1
      })
   })
   .then(response => response.json())
   .then(json => console.log(json));

   //[Section] Update a specific Post

   //Put Method

   fetch('https://jsonplaceholder.typicode.com/posts/1', {

      method: "PUT",
      headers: {
         'Content-type' : 'application/json'
      },
      body: JSON.stringify({  
               title: 'Updated Post',           
            })
   })
   .then(response => response.json())
   .then(json => console.log(json))

   //patch

   fetch('https://jsonplaceholder.typicode.com/posts/1', {

      method: "PATCH",
      headers: {
         'Content-type' : 'application/json'
      },
      body: JSON.stringify({
               title: 'Updated Post',
            })
   })
   .then(response => response.json())
   .then(json => console.log(json))

   //The PUT method is method of modifying resource where the client ends data that updates the entire object/ducoment

   //Patch method applies a partial update to the object or document.

   //[Section] Deleting a post

   //deleting specific post following the REST API
   fetch('https://jsonplaceholder.typicode.com/posts/1', {method : "DELETE"})
   .then(response => response.json())
<<<<<<< HEAD
   .then(json => console.log(json));
=======
   .then(json => console.log(json));
>>>>>>> ca52afae2b9e46353e51568515f0ba3c44fe8fc5
