// Single Line Comment

/*
   Multi line Comment
   This is a comment

*/

// [Section] syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell the computer to perform
// Javascript Stament ussually it ends with semicolon(;)
// Semicolons are not required inJS, but we will use it to help us prepare for the other stric languages like Java.
// A syntax in the programming, it it the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific or else the statement will not run.

// [section] variables
// it it used to contain/store data.
//any information that is used by an application is stored in what we call memory.
// when create variables, certain portion of a device memory is given a "name" that we call "variable.

// Declaring Variables
// Declaring variables - tells our device that a variable name is created and is ready to store data
    //syntax:
       // let/const variableName

let myVariable;

// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log() is useful for printing values of a variabl or certain results of code into the Google Chromes Browser's console
console.log(myVariable);


/*

   Guides in writing variables:
   1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator (=) to assign a value.
   2.Variable names should start with a lowercase character, use cameCAse for multiple keyword.
   3.For constant variables, use 'const' keyword.
   4.Variable nems, it should be indicative (descriptive) of the value being stored to avoid confusion.

*/ 

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given its's initial or starting value.
    // syntax
      // let/const variableName =value;

// example:
let productName = 'desktop computer';

console.log(productName);

let producPrice = 18999;
console.log(producPrice);

// In the context of certain applications, some variables/ information are constant and should not change.
// In this example, the interest rate for a loan or savings account or a mortgage must not change due to real world concerns.

const interest = 3.539

// Reassigning Variable values
// Reassigning a variable, it means chaning it's initial or previous into another value.
   // syntax
    // variableName = newValue

productName = 'laptop';
console.log(productName);

// The value of a variable declared using the const keyword can't be reassigned.

/*interest = 4.489;
console.log(interest);*/

// reassigning variables vs. initializing variables
// declares a variable

let supplier;
// Initializing
supplier="John Smith Trading";
// reassigning
supplier="Uy's Trdaing";

// Declaring and initializing
let consumer ="Chris";

// Reassigning
consumer ="thoper";

// Can you declare a const variable without initialization.
    // no. an error will occur.

/*const driver;

driver = "warlon Jay"*/;

const driver = "Warlon Jay"

// var vs. let/const kyeword
    // var - is also is used in declaring variables. but var is an EcmaScript 1 version (1997)
    // let/const keyword was introduced as a new feature is Es6(2015)

// What makes let/const different from var?
   // there are issues associated with variables declared/ created using var, regarding hoisting
   // Hoiting is JavaScript default behavior of moving declarations to the top.
   // in terms if a variables and constants, keyword var is hoisted and let and const does not allow hoisting.

// Example of Hoisted:

a = 5;
console.log(a);

var a;

/*b = 6;
console.log(b);
let b;*/

// let/const local or global scopes
   // Scope essentially means where these variables are available or accessible for use

   // let and const are block scoped
   // A block is a chunk of code bounded by {}. A block lives i an curly braces. anything within the braces are block

let outerVariable = "hello";

let globalVariable;

{
	let innerVariable =" hello again";
	console.log(innerVariable);
	console.log(outerVariable);

	globalVariable = innerVariable;
}

// console.log(innerVariable);

console.log(globalVariable);

// Multiple variable declarations and initialization.
// Multiple variables may be declared in one statement

let productCode="CD017", productBrand = "Dell"
console.log(productCode);
console.log(productBrand);

// Multiple variables to be consoled in one line.
console.log(productCode, productBrand);

// using a variable with a reserved keyword.
// reserved keywords cannot be used as a variable name as it has function in Javascript.
// const let = "Hi Im let keyword";

// console.log(let);

// [Section]Data types

// strings
// strings are series of characters that create a word, a phrase, a sentence or anything related to creating text.
// string in Javascript can be written using iether a single ('') or double ("") quote

let country = 'Philippines';
let province = "Metro Manila";
   // Contenation of the strings in JavaScript
      // Multiple string values can be combined to create a single string the "+" symbol

let fullAddress = province 
+ ', ' + country
console.log(fullAddress);

let greeting = 'I live in the' + country;
console.log(greeting);

/*
  -The escape characters (\) in strings in combination with other characters can produce different effects/results.
  -"\n" this creates a next line in between line in the text.

*/

let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message);

// Numbers
// Integers/whole numbers
let headcount = 16
console.log(headcount);

// Decimal Number/Fractions
let grade = 95.7;
console.log(grade);

// Exponentiak Notation
let planetDistance = 2e10
console.log(planetDistance);

// Combining text and Strings
console.log("John's grade last quarter is " +grade);

// Boolean
// Boolean values are normally used to create values relating to the state of certain things.
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried" +isMarried);
console.log("isGoodConduct: "+ isGoodConduct);

// Arrays
// Arrays are special kind of fata that's used to store multiple related values.
// In javascript, Arrays can store diffirent data types but is normally used to store similar data types.

// similar data types
// syntax:
   // let/const arrayName = [elementA, elementB, elementC, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
let details = ["John", "Smith", 32, true];
// not recommend in using array
console.log(details);

// Objects
  //Objects are another special kind of data type that's use to mimic real world objects/items.
  // 

  /*
  
    Syntax:
    let/const objectName = {
	    propertyA: valueA,
	    propertyB: valueB
    }

  */ 


   let person = {
      fullName: "Juan Dela Cruz",
      age: 35,
      isMarried: false,
      contact: ["+63917 123 4567", "8123 4567"],
      address: {
         houseNumber: "345",
         city: 'Manila'
      }
   }

   console.log(person);

   // type of operator, is used to determine the type of data or value of variable. it outputs string

   console.log(typeof mailAddress);
   console.log(typeof headcount);
   console.log(typeof is isMarried);

   console.log(typeof grades);

   // Note: Array is a special type of object with methods and function to manipulate.

   // Constant Objects and Arrays

   /*The keyword const is a lottle misleading

   it does not define a constant value. it defines a constant reference to a value:

   Because of this you can not:
   Reassign a constant value:
   Reassign a constant object:

   But you can:

   change the elements of a constant array.
   change the proporties of constant project.

   */

const anime = ["One piece", "One Punch Man", "Attack on Titan"];

/* const anime = ["One piece", "One Punch Man", "Kimetsu no yaiba"];
   console.log(anime);
*/

anime[2] = "Kimetsu no yaiba";
console.log(anime);

// Null
// It is used to intentionally express the absence of a value in a variable.

let spouse = null;

spouse = "Maria";

// Represents the state of a variable that has been declared but without an assigned value.

let fullName;

fullName = "Maria";