// console.log("Good afternoon, Batch 288!");

//Array Methods
	// javaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	//Mutator Methods
	// Mutators methods are functions that "mutate" or change an array after they're created
	//These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			-Adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');

	console.log(fruitsLength);
	console.log('Mutated array from push method: ');
	console.log(fruits);

	// Pushing multple elements to an array

	fruitsLength = fruits.push('Avocado', 'Guava');

	console.log(fruitsLength);
	console.log("Mutated array after pushing multiple elements:");
	console.log(fruits);


	// pop()
	/*
		-removes the last element AND returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruit = fruits.pop();

	console.log(removedFruit);
	console.log("Mutated Array from the pop method:");
	console.log(fruits);

	//unshift()
	/*
		-it adds one or more elements at the beginning of an array AND it returns the update array length
		-Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB ' . . .)
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');

	console.log(fruitsLength);
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	//shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element.
		-syntax:
			arrayName.shift();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.shift();

	console.log("Mutated array from the shift method: ");
	console.log(fruits);

// splice()
/*
  -Simultaneously removes an elements from a specified index number and adds element
  -Syntax
    arrayName.splice(startingIndex, deletecount, elementsToBeAdded)
*/

console.log("Current Array: ");
console.log(fruits);

fruits.splice(4, 2, "lime", "Cherry");
console.log("Mutated array after the splice method:");
console.log(fruits);


//sort()
   /*
     Rearranges the array elements in alphanumeric order
      -syntax:
      arrayName.sort();
   */

console.log("Current Array: ");
console.log(fruits);

fruits.sort();

console.log("Mutated array from the sort method: ");
console.log(fruits);


//reverse()
     /*
       -reverses the order of array elements
       -syntax:
           arrayName.reverse(); 
     */

console.log("Current Array");
console.log(fruits);

fruits.reverse();
console.log("Mutated array from the reverse method")
console.log(fruits);

//[Section] Non-mutator methods
/*
  -non-mutator methods are functions that do not modify or change an array after there craeted
  -these methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output.
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

//indexOf()
//returns the index number of the first matching element found in an array.
// If no match was found, the result will be -1.
// the search process will be done from the first elemnt proceeding to the last element
/*
  syntax:
     arrayName.indexOf(searchValue);
     arrayName.indexOf(searchValue),
     startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log(firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log(invalidCountry);

firstIndex = countries.indexOf('PH', 2)
console.log(firstIndex);

console.log(countries);

// lastIndexOf()
/*
  -returns the index number of the last matching element found in an array
  -the search process will be done from last element proceeeding to the first element
     syntax:
     arrayName.lastIndexOf(searchValue);
     arrayName.lastIndexOf(searchValue,startingIndex);


*/

let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);

invalidCountry = countries.lastIndexOf('BR');
console.log(invalidCountry);

lastIndexOf = countries.lastIndexOf("PH", 6);
console.log(lastIndexOf);

//indexof, starting from the startig index going to the last element(from left to right);
//lastindexof, starting from the starting index goint to the first element(from right to left);

console.log(countries);

//slice()
   /*
      -portions/slices elements from an array And return a new array
      -syntax:
           arrayName.slice(startingIndex);
           arrayName.slice(statingIndex, endingIndex);
   */

   // Slicing off elements from a specified index to the last element.

   let slicedArrayA = countries.slice(2);
   console.log('Result from slice method:');
   console.log(slicedArrayA);

   //Slicing off elements from a specified index to another index:
   // the elements that will be sliced are elements from the starting index until the element before the ending 
   let slicedArrayB = countries.slice(2, 4);
   console.log('Result from slice method:');
   console.log(slicedArrayB);

   //slicing off elements starting from the last element of an array;
    let slicedArrayC = countries.slice(-3);
   console.log('Result from the slice method:');
   console.log(slicedArrayC);

   //toString()
   /*
      returns an array as string seperated by commas
      syntax:
      arrayName.toString();
   */

    let stringArray = countries.toString();
    console.log('Result from toString method:')
    console.log(stringArray);

    console.log(typeof stringArray);

    //concat()
    //combines array to an array or elemnts and returns the combined result.
    //Syntax:
       //arrayA.concat(arrayB);
       //arrayA.concat(elementA);

    let taskArraysA = ["drink HTML", "eat savascript"];
    let taskArraysB = ["inhale css", "breathe sass"];
    let taskArraysC = ["get git", "be node"];

    let tasks = taskArraysA.concat(taskArraysB);
    console.log(tasks);

    let combinedTasks = taskArraysA.concat('Smell express', 'Throw react');
    console.log(combinedTasks);

    //concat multiple array into an array

    let alltasks = taskArraysA.concat(taskArraysB, taskArraysC);
    console.log(alltasks);

    // concat array to an array and element
    let exampleTasks = taskArraysA.concat(taskArraysB, 'smell express');
    console.log(exampleTasks);

    //join()
    // returns an array as string seperated by speciefied seperator string
    // syntax:
       // arrayName.join("seperatorString")

    let users = ['John', 'Jane', 'Joe', 'Robert'];

    console.log(users.join());
	console.log(users.join(''));
	console.log(users.join(" - "));

// [section] iteration methods
  // iteration methods are loops designed to perform repititive task
  //Iteration methods loops over all items in an array 

  //forEach()
  //Similar to a for loop that iterates on each of array element.
  // syntax:
	//arrayName.forEach(function(indivElement){statement};)

	console.log(alltasks);
	//["drink HTML", "eat savascript", "inhale css", "breathe sass", "get git", "be node"];

	alltasks.forEach(function(task){
		console.log(task)
	});

    //filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters.
let filteredTasks = [];

alltasks.forEach(function(task){
	if(task.lenght > 10) {
		filteredTasks.push(task);
	}
})
console.log(filteredTasks);


//map()
// iterates on each element and returns new array with different values depending on the result os the function's operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * 3;

})
console.log(numbers);
console.log(numberMap);

//every()
/*
  it will check if all elements in an array meet thegiven the condition
  -return true value if all elements meet the condition and false otherwise

  Syntax:
      let/const resulArray = arrayName.every(function(indivElements){
	  return expression/condition;
      })
*/

numbers = [1, 2, 3, 4, 5];

let allValid = numbers.every(function(number){
     return (numbers<6);
})

console.log(allValid);

//some()
// checks if at least one element in the array meets the given condition.
/*
  syntax:
  let/const resultArray = arrayName.some(function(indivElement){
	return expression/condition;
  })

*/

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log(someValid);

//filter()
// returns new array that contains elements which mets the givven
//returns an empty array if no elements were found
/*
   syntax:
      let/const resultArray = arrayName.filter(
      function(indivElement){
	return expression/condition;
      })
*/

numbers = [1, 2, 3, 4, 5];

let filteredValid = numbers.filter(function(number){
	return (number % 2 === 0);
})

console.log(filteredValid);

// includes()
   // checks of the argument passed can be found in the array

	let products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	// reduce()
		// evaluates elements from left to right and returns/reduces the array into single value
	numbers = [1, 2, 3, 4, 5];
	// The first parameter in the function will be accumulator
	//the second parameter in the function will be the currentValue
	let reducedArray = numbers.reduce(function(x, y){
		console.log('Accumulator: ' + x);
		console.log('currentValue: ' + y);
		return x*y;
	})

	console.log(reducedArray);

	 let sales = [190, 12387, 12381283, 1298312, 1928312];


	products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

	let  reducedString = products.reduce(function(x, y){
		return (x+y);
	})

	console.log(reducedString);
