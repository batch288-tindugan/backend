//console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
const trainer = {
    Name: "Ash Ketchum",
    Age: 10,
    Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    Friends: { 
        hoenn: ["May", "Misty"],
        kanto: ["Brock", "Misty"], 
        },
    Talk: function() {
        return "Pikachu! I choose you!";
    } 
};
console.log(trainer);
console.log('Result of dot notation: ' + trainer.Name);

const Square = "Pokemon";
console.log('Result of square bracket notation: ');
console.log(trainer[Square]);

console.log('Result of talk method');
console.log(trainer.Talk());
 
// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level) {
   this.Name = name;
   this.Level = level;
   this.Health = level * 2;
   this.Attack = level;

   this.tackle = function(target){
    target.Health -= this.Attack;
    console.log(this.Name + " tackled " + target.Name);
    if (target.Health <= 0){
        return this.faint(target);
    }
    else{
        return `${target.Name} 's health is now reduced to ${target.Health}`;
    };
   };

   this.faint = function(target) {
       return target.Name + " has fainted";
  };
}

// Create/instantiate a new pokemon

let pikachu = new Pokemon("Pikachu", 12)
console.log(pikachu);

// Create/instantiate a new pokemon

let geodude = new Pokemon("Geodude", 8)
console.log(geodude);

// Create/instantiate a new pokemon

let mewtwo = new Pokemon("Mewtwo", 100)
console.log(mewtwo);

// Invoke the tackle method and target a different object
geodude.tackle(pikachu);
mewtwo.tackle(geodude);


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}