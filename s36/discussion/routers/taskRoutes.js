const express = require("express");

const taskControllers = require('../controllers/taskControllers.js');

//Contain all the endpoints of our application
const router = express.Router();


router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks);

//Parameterizer

//We are create a routw using delete method at the URL "/task/:id"
//The colon here is an indentifier that helps to create a dynamic route which allows us to supply information.
router.delete("/addTask", taskControllers.addTasks);



module.exports = router;

router.get("/:id",taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.updateSpecific);