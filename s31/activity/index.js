// What directive is used by Node.js in loading the modules it needs?

//answer:
  //Require directive is used to load a Node module(http) and store returned its instance(http) into its variable(http)

// What Node.js module contains a method for server creation?

//answer:
   //The http module contains the function to create the server, which we will see later on. If you would like to learn more about modules in Node.

// What is the method of the http object responsible for creating a server using Node.js?

//answer:
  //The http. createServer() method creates an HTTP Server object. The HTTP Server object can listen to ports on your computer and execute a function, a requestListener, each time a request is made.


// What method of the response object allows us to set status codes and content types?

//anwer:
  //Setting Status Codes
  //You simply call the response objects status method. res. status(404). send("Not Found");

// Where will console.log() output its contents when run in Node.js?

//answer:
   //The console. log() method outputs a message to the web console. The message may be a single string (with optional substitution values), or it may be any one or more JavaScript objects.

// What property of the request object contains the address' endpoint?

//answer:
   //The req object represents the HTTP request and has properties for the request query string, parameters, body, HTTP headers, and so on.


const http = require('http');

const server = http.createServer((request, response) => {
	if(request.url === '/login') {
		console.log('You are in the login page');
	}
	else {
		response.statusCode = 404;
		response.end('Error: Page not Found');
	}
});

const port = 3000;

server.listen(port, () => {
	console.log('server is running sucessfully running');
});