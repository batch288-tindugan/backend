const express = require('express');
//Mongoose is a package that allows creation of schemas to Model our dta structures
//Also has access to a number of methods for manipulation our database
const mongoose = require('mongoose');


const port = 3001;

const app = express();

     //Section: MongoDB Connection
     //Connect to the database by passing your connection string
     //Due to update in Mongo DB drivers that allow connection, the default connection string is being flagged as an error
     //by default a warning will be displayed in the terminal when the application is running.
     //{newUrlParser:true}


     //syntax:
     //mongoose.connect("MongoDB string",{ userNewUrlParser:true});

mongoose.connect("mongodb+srv://admin:admin@batch288tindugan.jnr8rvm.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

     //Notification whether the we connected properly with the database.

let db = mongoose.connection;

     //for catching the error just in case we had an error during the connection
     //console.error.bind allows us to print error in the browser and in the terminal
db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

     //if the connection is succesful:
db.once("open", ()=> console.log("We're connected to the cloud database!"));

     //[Section] mongoose schemas
     //Schemas deteremines the structure of the document to be written in the database
     //Schemas act as blueprint to our data
     //Use the Schema() constructor of the mongoose module to create a new Schema object.

const taskSchema = new mongoose.Schema({
     	//Define the fields with the corresponding data type
    name: String,
     	//let us another field which is status
    status: {
        type: String,
   default: "pending"
   }
})

     //[Section] Models
     // Uses schema and are used to create/instantiate objects that corresponds to the schema.
     //Models use Schema and they act the middleman from the server(JS code) to our database.

     // To create a model we are going to use the model();

const Task = mongoose.model('Task', taskSchema);

     //Section: Routes 

     //Create a Post route to create a new task
     //Create a new task 
     //Business Logic:
         //1.Add a functionality to check whether there are duplicate task
             //if the task is existing in the databas, we return an error
             //if the task does exist in the database, we add it in the database
          //2. the task data will be coming from the request's body

app.post("/task", (request, response) => {
          //"findOne" method is a mongoose method that acts similar to "find" in MongoDB.
          //if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will return an empty object or null.
     console.log(request.body);
     Task.findOne({name: request.body.name})
     .then(result => {
             //we can use if statement to check or w=verify whether we have an object found.
       if(result !== null){
          return response.send("Duplicate task found!");
     }
     else{
         
               //Create a new tsak and save it to the data base.
          let newTask = new Task ({
               name: request.body.name
          })

               //The save() method will store the information to the database
               //since the newTask was created from the mongoose schema and task model, it will be save in tasks collection
          newTask.save()

          return response.send("New task created!")

     }

})
})

//Middlewares
// Allows our app to read json data
app.use(express.json())
//allows our app to read data from forms
app.use(express.urlencoded({extended:true}))


app.listen(port, () => {
	console.log(`server is running at port ${port}!`);
})


const newSchema = new mongoose.Schema({
   username: String,
   password: String
});

const users = mongoose.model('Users', newSchema);

app.post("/signup", (request, response) => {
   console.log(request.body);
   users.findOne({ username: request.body.username })
   .then(result => {
    if (result !== null) {
       return response.send("Duplicate username Found!");
  } else {
       if (request.body.username && request.body.password) {
          let newUser = new users({
             username: request.body.username,
             password: request.body.password
        });

          newUser.save()
          .then(() => {
           response.status(201).send("New user registered");
      })
          .catch(error => {
           response.send(error);
      });
     } else {
          response.send("BOTH username and password must be provided");
     }
}
})
   .catch(error => {
    response.send(error);
});
});


if(require.main === module){
     app.listen(port, () =>{ console.log(`Server running at port ${port}`)
})   
}
module.exports =app;