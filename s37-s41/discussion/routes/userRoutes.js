const express = require("express");
const userControllers = require('../controllers/userControllers.js');

const auth = require ('../auth.js')

const router = express.Router();

//Routes
router.post("/register", userControllers.registerUser)

//login
router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getProfile);

router.get("/userdetails", auth.verify, userControllers.retrieveUserDetails);

//route for course enrollment
router.post('/enroll', auth.verify, userControllers.enrollCourse)

module.exports = router;