const User = require("../models/Users.js");

const Courses = require("../models/Courses.js");

const bcrypt = require("bcrypt");

const auth = require('../auth.js');

//Controllers

//Create a controller for the signup
//registerUser
/*Business Logic/ Flows*/
   //1. First, we have to validate whether the user is existing or not. we can do that by validating whether the email exist on our databases or not.
   //2. if the user email is existing, we will prompt an error telling the user that the email is taken.
   //3.otherwise, we will sign up or add the user in our database.
module.exports.registerUser = (request, response) => {
   //find method: it will return the an array of object taht fits the given criteria
   User.findOne({email : request.body.email})
   .then(result => {

   	   //we need add if statement to verify whether the email exist already in our database
   	   if (result) {

   	   	return response.send(false);
   	   }else{

   	   	  //Create a new Object instantiated using User Model.
   	   	  let newUser = new User({
   	   	  	  firstName: request.body.firstName,
   	   	  	  lastName: request.body.lastName,
   	   	  	  email: request.body.email,
              //hashSync method : it hash/encrypt our password
              //the second argument salt rounds
   	   	  	  password: bcrypt.hashSync(request.body.password, 10),
   	   	  	  mobileNo: request.body.mobileNo
   	   	  })

   	   	  //Save the user
          //Error handling
   	   	  newUser.save()
   	   	  .then(saved => response.send(true))
   	   	  .catch(error => response.send(false))
   	   }
   })
   .catch(error => response.send(false));
}

//new controller for the authentication of the user
module.exports.loginUser = (request, response) => {

	User.findOne({email: request.body.email})
	.then(result => {
       
       if(!result){
       	   return response.send(false)
       }else{
       	   //The compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison
           const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

           if(isPasswordCorrect){
           	  return response.send({auth: auth.createAccessToken(result)})

           } else{
           	  return response.send(false)
           }
       }
	})
	.catch(error => response.send(false));
}


module.exports.getProfile = (request, response) => {
    
   User.findById(request.body.id)
   .then(result => {

      const idDetails = auth.decode(request.headers.authorization);

        if(idDetails.isAdmin){
          password: result.password = `*******`
         return response.send(result)
        } else {
         return response.send("You are not an admin, you don't have access to this route.")
        }

   }).catch(error => response.send(error))
}

//Controller for the enroll course.
module.exports.enrollCourse = async (request, response) => {
   
   const courseId = request.body.id;

   const userData = auth.decode(request.headers.authorization);

   if(userData.isAdmin){
      return response.send(false);
   } else {

      //Push papunta kay user document
      let isUserUpdated = await User.findOne({_id: userData.id})
      .then(result => {

         result.enrollments.push({
            courseId: courseId
         })
         console.log(result);

          return result.save()
         .then(saved => true)
         .catch(error => {
         console.log(error);
         return false
      })
      })
       .catch(error => {
         console.log(error);
         return false
      })


      //push papunta kay course documents
      let isCourseUpdated = await Courses.findOne({_id : courseId})
      .then(result => {

         result.enrollees.push({userId : userData.id})   
  
       return result.save()
      .then(saved => true)
      .catch(error => {
         console.log(error);
         return false
      })
      })
      .catch(error => {
         console.log(error);
         return false
      }) 


    //if condition to check wether we updated the users document and courses document

    if (isUserUpdated && isCourseUpdated) { 
      return response.send(true);
    }else {
      return response.send(false);
    }
 }
}

module.exports.retrieveUserDetails = (request, response) => {
   const userData = auth.decode(request.headers.authorization);

   User.findOne({_id : userData.id})
   .then(data => response.send(data));
}