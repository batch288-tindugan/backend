const express = require("express");
const mongoose = require('mongoose');

//it will allows our backend application to be available to our frontend application
//it will also allows us to control the app's Cross Origin Resource Sharing settings.

const cors = require('cors');

const userRoutes = require('./routes/userRoutes.js');

const coursesRoutes = require('./routes/coursesRoutes.js');

const app = express();

const port = 4001;

//Mongod DB connection
mongoose.connect("mongodb+srv://admin:admin@batch288tindugan.jnr8rvm.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {useNewUrlParser: true,
	useUnifiedTopology: true
})


let CourseBookingAPI = mongoose.connection

CourseBookingAPI.on("error", console.error.bind(
    console,"Error! can't connect to the database"));


CourseBookingAPI.once("open", ()=> console.log("We're connected to the cloud database!"))

//middlewares
app.use(express.json());

app.use(express.urlencoded({extended:true}));

//Reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

//add the routing of the routes from the userRoutes

app.use('/user', userRoutes);
app.use('/courses', coursesRoutes)




app.listen(port, () => {
	console.log(`server is running at port ${port}!`);
 })