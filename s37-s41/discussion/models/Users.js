const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "User's first name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "User's last name is required!"]
	},
	email: {
		type: String,
		required: [true, "User's email is required!"]
	},
	password: {
		type: String,
		required: [true, "User's password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: true
	},
	enrollments: [
            {
            	courseId: {
            		type: String,
            		required: [true, "CourseId of enrollee is required!"]
            	},
            	enrolledOn: {
            		type: Date,
            	    default: new Date()
            	},
            	status: {
            		type: String,
            	    default: "Enrolled"
            	}
            }
		]
})

const User = mongoose.model("User", UserSchema);

module.exports = User;