// console.log("Hello World");

// [Section] While loop
  // A while loop takes an expression/condition. expressions are any unit code that can be evaluated as true or false. if the condition evaluates to be true, the statements/code block will be executed.
  // A loop will iterate a certain number of times until an expression/condition is true.
  // Iteneration is term given to repitition of statements.
/*
   syntax:
      while(expression/condition){
      statement;
      increment/decrement;
      }
*/

let count = 5;

    // While the value of count is not equal to zero the statement inside will run/iterate
while(count !== 0){

      // The current value of count is printed out
  console.log( "while :" + count);

      // Decrease the value of count by after every itenation to stop the loop when it reaches 0.
      // Forgetting to include this in our loops will make our applications run an infinite loop which will eventually crash device.
      // After running the script, if a slow response from the browser is experienced or an infinte loop is seen int he console quickly close the application/browser/tab to avoid this.

  count--;
}

    // [Section] Do-While Loop

    /*
      -A do while loop works a lot like the while loop.
      but unlike while loops, do-while loops guarantees that the code will be executed at least once.

      syntax:
        do{
           statement;
           increment/decrement;
        } while (condition/expression)
    */

 /*let number = Number(prompt("Give me a number:"));
    // the number function works similarly to the "parseint" function.

    do{
      console.log("Do While: " + number)

       // Increment
      number++;
    } while (number < 10)*/

    // [Section] For loop
     /*
       A for loop is more flexible than while and do-while loops. it consist of three parts:
         1. the "initilization" value that will track the progression of the loop.
         2.expression/condition that will be evaluated which will determine whether the loop will run one more time.
         3.the "iteration" which indicates how to advance the loop.

         syntax:
         for(initialization; expression/condition;
         iteration){
            statement;
         }
     */ 


    /*
      Will create a loop that will start from 0 and will end at 20
      Every iteration of the loop, the value of count will be checked if it is equal or less than 20 
      if the value of count is less than or equal 20 the statement inside the loop will execute.
      the value of count will be incremented by one for each iteration.
    */ 
for(let count = 0; count <= 20; count++){
  console.log("the current value of count is " + count);
}

let myString = "alexis";
    // .length property
    //  Characters is strings may be counted using the .length property.

console.log(myString.length);


    // Access elements of a string/ to the characters of the string.

console.log(myString[0]);
console.log(myString[3]);
    // Since the last index in our string is 3, therefore the mystring[5] will output an undefined.
console.log(myString[5]);

    // We will create a loop that will print out the individual letters of the myString variable;

for(let index = 0; index < myString.length; index++){
 console.log(myString[index])
}


    // Create a string named "myName" with value of Alex;

let myName = "Alex";

    /*
      Create a loop that will print out the letter of the name individually and printout the number 3 instead when the to be printed is a vowel.
    */

for (let index= 0; index < myName.length; index++) {

      // console.log(myName[index]);
  if(myName[index] === "a" || myName[index] === "e" || myName[index] === "i" || myName[index] === "o" || myName[index] === "u"){

   console.log(3);
 }else{
  console.log(myName[index]);
}
}

let myNickName = "Chis Mortel";

let updatedNickName = myNickName.replace("Chis", "Chris");
console.log(myNickName);
console.log(updatedNickName);

   // [Section] Continue and Break Statements
      // the "continue" statements allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
      // the "break" statement is used to terminate the current loop once a match has been found.

   // Create a loop that if the count value is divided by and the remain is 0, it will print the number and continue to the next iteration of the loop.

console.log("----------------------------------------")

for(let count = 0; count <= 20; count++){

 
 if(count >=10){
  console.log("The number is equal to 10, stopping the loop")
  break;
}
if(count % 2 === 0){
  console.log( "the number is divisible to two, skipping . . ." )
  continue;
}
console.log(count);

}

// Create a loop that will iterate based on the length og the string name;

let name = "alexandro";

    // we are going to console the letters per line, and if the letter is equal to "a" we are going to console "Continue to the next iteration" then add continue statement.
    // if the current letter is equal to "d" we are going to stop the loop.
console.log("-------------------------------------");
for(let index = 0 ; index < name.length; index++){

    if(name[index] === "a"){
      console.log("Continue to the next iteration!")
      continue;
    }else if(name[index] === "d"){
      break;
    }else{
      console.log(name[index]);
    }

  }
