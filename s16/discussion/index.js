// To check whether the script file is properly linked to your html file.
/*console.log("Hello batch 288!");*/

// [Section] Arithmetic Operations
  let x = 1397;
  let y = 7831;


  // Addition Operator (+)

 let sum = x + y;

 console.log("Result of addition operator: " + sum);

 // Substraction operator (-)
 let difference = y - x;
 console.log("Result of subtraction opertaor: " + difference);

 // multiplication operator (*)
 let product = x * y;
 console.log("Result of multiplication operator: " + product);

 // division operator (/)
 let quotient = y / x;
 console.log("Result of division operator: " + quotient. toFixed(2));

 // MOdulo Operation (%)
 let remainder = y % x;
 console.log("Result of modulo operator: " + remainder);

 // [Section] Assignment Operators

   // Assignment Operator (=)
   // The assignment operators assigns/reassigns the value to the variable.

   let assignmentNumber = 8;

   // Addition Assignment Ooperator (+=)
   // The addition assignment operator adds value of the right operand to a variable and assigns the result to the variable.

   assignmentNumber += 2

   console.log(assignmentNumber);

   assignmentNumber += 3;
   console.log("Result of the addition assignment operator: " + assignmentNumber);

   // Subtraction/Multiplication/Division assignments operator (-=, *=, /=)


   // subtraction assignment operation

   assignmentNumber -= 2;
   console.log("result of subtarction assignment operator " + assignmentNumber);

    // Multiplication assignment operator

   assignmentNumber *= 3;
   console.log("result of multiplication assignment operator " + assignmentNumber);

   // Division assignment operator

   assignmentNumber /= 11;
   console.log("Result of division assignment operator " + assignmentNumber);

 // Multiple operators nad parenthesis 
    // MDAS - Multiplication or Division First the additon or Substraction, From left to right.

   let mdas = 1 + 2 - 3 * 4 / 5;

  /*
     1. 3 * 4 = 12
     2. 12 / 5 = 2.4
     1 + 2 - 2.4
     3. 1 + 2 = 3
     4. 3 - 2.4 = 0.6 
  */ 
   console.log(mdas.toFixed(1));

   let pemdas = 1 + (2-3) * (4/5);

   /*
     1. 4/5 = 0.8
     2. 2-3 = -1
     1 + (-1) * (0.8)
     3. -1 * 0.8 = - 0.8
     4. 1 - 0.8
     0.2  
   */

   console.log(pemdas.toFixed(1));

   // [Section] Increment and Decrement
     // Operators that add or subtract values by 1 and reassigns decrement applied to.

   let z = 1;

   let increment = ++z;
   console.log("Result of pre-increment: " + increment);

   console.log("result of pre-increment: " + z);

   increment = z++;
   console.log("the result of post-increment: " + increment);

  console.log("the result of post-increment: " + z);

 x = 1;

 let decrement =--x;

 console.log("result of pre-decrement: " + decrement);
 console.log("result of pre-decrement: " + x);

 decrement = x--;
 console.log("Result of post-decrement: " + decrement);
 console.log("Result of post-decrement: " + x);


// [Section] Type Coercion

 /*
   -type coercion is the automatic or implicit conversation of values from one data type to another.
   -this happens when operations are performed on diffirent data types that would normally possible and yield irregular result.
  */


  let numA ='10';
  let numB = 12;

  let coercion = numA + numB;
  // If you are going to add string and number, it will concatenate its valuje.
  console.log(coercion);
  console.log(typeof coercion);
 

  let numC = 16;
  let numD = 14;


  let nonCoercion = numC + numD;
  console.log(nonCoercion);
  console.log(typeof nonCoercion);
 

// the boolean "true" is also associated with the value of 1
 let numE = true + 1;
 console.log(numE);

// The boolean "false" is also associated with the value 0
let numF = false + 1;
console.log(numF);

//[Section] Comparison Operators

let juan = 'juan';

// Equality Operator (=)
/*
   - Checks whether the operand are equal/have the same content/value.
   -attempts to CONVERT AND COMPARE operands of different data types.
   -Returns a boolean value.
*/

console.log(1 == 1);  //true
console.log(1 == 2);  //false
console.log(1 == "1"); //true
console.log(0 == false); //true
// compare two stringd that are the same
console.log('juan' == 'juan'); //true
console.log('JUAN' == 'juan'); //false (case-sensitive)
console.log(juan == 'juan'); //true


// Inequality Operator (!=)
/*
  -check whether the operands are not equal/have different content/values.
  -attempts to CONVERT AND COMPARE operands of different data types.
  -Returns Boolean value.
*/

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(0 != false); //false
console.log('JUAN' != "juan"); //true
console.log(juan != "juan"); //false

// Strict Equality Operator (===)

/*
   -Checks whether the operands are equal/have the same content.
   -Also COMPARE the data types of the values.

*/

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1") //false
console.log(0 === false); //false
console.log('JUAN' === 'juan'); //false
console.log(juan === 'juan'); //true

// Strictly Inequality Operator
/*
   -checks wether the operands are not equal/ have different content
   -Also COMPARES the data types of 2 values.

*/
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true  
console.log(0 !== false); //true
console.log('JUAN' !== 'juan'); //true
console.log(juan !== 'juan'); //false

// [SECTION] Relational Operators
// Checks whether one value is grater or less than to the other values.

 let a = 50
 let b = 65

  // GT or Greater that Operator (>)
  let isGreaterThan = a > b; //false

  // LT or Less Than Operator (<)
  let isLessThan = a < b; //true

  //GTE or Greater Than or Equal Operator (>=)
  let isGToEqual = a >= b; //false

  //LTE or Less Than or Equal (<=)
  let isLTorEqual = a <= b; // true


  console.log(isGreaterThan);
  console.log(isLessThan);
  console.log(isGToEqual);
  console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr); //true
console.log(b <= numStr); //false

let strNum = "twenty";
console.log(b >= strNum); //false
// since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number);

// [SECTION] Locigal operators

 let isLegalAge = true;
 let isRegistered = false;


  // logical AND operator (&& - Double Ampersand)
 // Returns "true" if all operands are true.
 // isRegistered = true;
 let allRequirementsMet = isLegalAge &&
 isRegistered;   //false
 console.log("Result of logical AND operator: " +allRequirementsMet);

 // logical OR Operator (|| - Double pipe)
 // Returns "True" if one of the operands are true
 // isLegalAge = false;
 let someRequirementsMet = isLegalAge || isRegistered;
 console.log("result of logical OR operator: " +someRequirementsMet);


// Logical NOT Operator (! - exclamation point)
// returns the opposite value.
let someRequirementsNotMet = !isRegistered;
console.log("Result of the Logical NOT Operator: " + someRequirementsMet);
